from collections import defaultdict 

##class represents an directed graph using adjency list representation 

class Graph:
    ##constructor 

    def __init__(self):
        ##default dictionary for graph
        self.graph=defaultdict(list)

    ##func to add edge to graph 
    def addEdge(self,u,v):
        self.graph[u].append(v)
    
    def DFSUtil(self,v,visited):
        ##for checking if visited or not 
        ##mark current node as visited 
        visited[v]=True 
        print(v)

        #recursively traverse for all vertices adjacent to this vertex 
        for i in self.graph[v]:
            if visited[i]==False:
                self.DFSUtil(i,visited)

    ##func to do DFS traversal .uses recurvise DFSUtil()
    def DFS(self):
        V=len(self.graph) ##total vertices

        
        ##mark all vertices as not visited 
        visited=[False]*(V)
        ##call DFSUtil() for all vertices 
        for i in range(V):
            if visited[i]==False:
                self.DFSUtil(i,visited)


