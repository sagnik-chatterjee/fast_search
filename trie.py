'''
trie for faster insert and search 

'''

class TrieNode:

    def __init__(self):
        self.children=[None]*26

        self.isEndofWord=False

class Trie:

    ##trie data structure class 
    def __init__(self):
        sef.root=self.getNode()

    def getNode(self):
        ##return new trie node 
        return TrieNode()

    def _charToIndex(self,ch):
        ##private func to convert key current character into index
        ##for 'a' to 'z'
        return ord(ch)-ord('a')

    def insert(self,key):
        '''if not present insert new key into trie.
        if key is prefix of trie node 
        just marks self node 
        '''
        crawl=self.root
        length=len(key)
        for level in range(length):
            index =self._charToIndex(key[level])

            #if current char is not present 
            if not crawl.children[index]:
                crawl.children[index]=self.getNode()
            crawl=crawl.children[index]
        ##mark last node as leaf 
        crawl.isEndofWord =True 

    def search(self,key):
        ##search key in the trie 
        ## returns true is key present in trie 
        ##else fails 
        crawl=self.root
        length=len(key)
        for level in range(length):
            index=self._charToIndex(key[level])
            if not crawl.children[index]:
                return False 
            crwal=crawl.children[index]
        
        return crawl!=None and crawl.isEndofWord

    